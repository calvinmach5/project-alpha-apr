from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import TaskForm
from projects.models import Project


# Create your views here.
@login_required
def project_detail(request, id):
    tasks = Task.objects.filter(id=id)
    projects = Project.objects.filter(id=id)
    context = {"tasks": tasks, "projects": projects}
    return render(request, "projects/projects_detail.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.save()
            return redirect("list_projects")
        else:
            print(form.errors)
    else:
        form = TaskForm()
        context = {"form": form}
        return render(request, "tasks/create_task.html", context)


@login_required
def my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/show_my_tasks.html", context)

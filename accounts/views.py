from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import AccountForm, SignUpForm


# Create your views here.
def account_login(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                context = {
                    "form": form,
                    "error": "Invalid credentials",
                }
                return render(request, "accounts/login.html", context)
    else:
        form = AccountForm()
        context = {"form": form}
        return render(request, "accounts/login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def account_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
            else:
                user = User.objects.create_user(
                    username=username, password=password
                )
                login(request, user)
                return redirect("list_projects")
    else:
        form = SignUpForm()
        context = {"form": form}
        return render(request, "accounts/signup.html", context)
